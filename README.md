Monto
=====

Monto is a simple framework for building disintegrated development environments (DDEs). A DDE provides editing services in a similar style to those of integrated development environments (IDEs) but in a way that minimises the barrier to entry for both users and service providers.

Monto was originally developed by a group led by [Tony Sloane](https://wiki.mq.edu.au/display/plrg/Anthony+Sloane) at Macquarie University and was described in a [paper](http://link.springer.com/chapter/10.1007/978-3-319-11245-9_12) and [presentation](https://speakerdeck.com/inkytonik/monto-a-disintegrated-development-environment) at the [2014 International Conference on Software Language Engineering](http://www.sleconf.org/2014/). It is now a community project with contributions solicited from the SLE community and beyond.

Information about Monto can be found on the following pages:

 * [architecture description](https://bitbucket.org/inkytonik/monto/src/default/wiki/architecture.md)
    * [configuration](https://bitbucket.org/inkytonik/monto/src/default/wiki/configuration.md)
    * [frontends](https://bitbucket.org/inkytonik/monto/src/default/wiki/frontends.md)
    * [product conventions](https://bitbucket.org/inkytonik/monto/src/default/wiki/products.md)
 * [Python reference implementation](https://bitbucket.org/inkytonik/monto/src/default/wiki/python.md)
 * [C implementation](https://bitbucket.org/inkytonik/monto/src/default/wiki/c.md)

Contributors
------------

Tony Sloane, Matt Roberts, Scott Buckley, Shaun Muscat (Macquarie University, Australia)

Sebastian Erdweg, Sven Keidel (TU Darmstadt, Germany)

Joey Ezechiëls, Eelco Visser (TU Delft, The Netherlands)
