This page describes products that Monto servers can return.
Since Monto does not prescribe any particular naming convention for
products or structure for product contents, this page acts as a central
point for developing community conventions.

Product Languages
-----------------

Wherever possible the language used for a product should use the full
lower-case name of the language in which the product content is written.
For example, the output of the Hoogle shell command contains Haskell
function signatures, so the language should be `"haskell"`.

If the product content is written in no particular formal language,
then the language name `"text"` should be used.

Products from Shell Commands
----------------------------

If the content of a product is coming from a shell command then our
convention is to use the name of the command in lower-case as the product
name and the text content of the output of the shell command as the product
contents.
For example, if the `hoogle` command is used to generate a product
containing Hoogle search results, then the name of the production would be
`"hoogle"` and the contents would be the Hoogle result output.

Tokenization Products
---------------------

This product describes the positions, length and category of tokens a given
source consists of. For instance this information can be used to implement
syntax highlighting for a programming language.

An example of a tokenization product of a Java file could look this. A
version message gets send to the servers like usual.

```json
{
   "source": "Foo.java",
   "language": "java",
   "contents": "public class Foo { ..."
}
```

The server that implements the tokenization answers in the following format.

```json
{
   "source": "Foo.java",
   "language": "java",
   "product": "tokens",
   "contents": [
     { "offset": 0,  "length": 6, "category": "modifier"   },
     { "offset": 7,  "length": 4, "category": "structure"  },
     { "offset": 12, "length": 3, "category": "identifier" },
     ...
   ]
}
```

In order for a frontend to be able to implement syntax highlighting, the
allowed values for the token category have to be element of a predefined
set. The Monto project proposes the following categories that are derived
from [vim](http://vimdoc.sourceforge.net/htmldoc/syntax.html#group-name).

```text
comment:        any comment

constant:       any constant
string:         a string constant, "this is a string"
character:      a character constant, 'c', '\n'
number:         a number constant, 234, 0xff
boolean:        a boolean constant, TRUE, false
float:          a floating point constant, 2.3e10

identifier:     any variable name

statement:      any statement
conditional:    if, then, else, endif, switch, etc.
repeat:         for, do, while, continue, break, etc.
label:          case, default, etc.
operator:       sizeof, +, *, etc.
keyword:        any other keyword
exception:      try, catch, throw

type:           int, long, char, etc.
modifier:       public, private, static, etc.
structure:      struct, union, enum, class etc.

punctuation:    any non alphanumeric tokens that are no operators.
parenthesis:    () [] {}, <> etc.
delimiter:      , . ; etc.

meta:           C preprocessor macros, Java Annotations, etc

whitespace:     Non visible character

unknown:        Unknown token, can occur during text insertion
```

The predefined categories are organized in hierarchies to make it easier
for users to build color schemes. For example each `number` literal is also
a `constant`. The user can now choose to set a color for all constants or
to give number literals a different color. The following graphic describes
visually the hierarchies of categories, where an arrow from `A` to `B`
means that `B` is-a `A`.

![Token Category Hierarchy](https://bitbucket.org/inkytonik/monto/raw/default/wiki/token_category_hierarchy.svg)
