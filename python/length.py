#! /usr/bin/env python3
# length
# Receive Monto versions and report their length.

import montolib

def version_length (version):
    return [('length', 'number', len (version['contents']), True)]

montolib.server (version_length)
