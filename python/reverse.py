#! /usr/bin/env python3
# reverse
# Receive Monto changes and report their reverse.

import montolib

def change_reverse (change):
    return [('reverse', 'text', change['contents'][::-1], True)]

montolib.server (change_reverse)
