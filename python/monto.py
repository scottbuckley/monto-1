#! /usr/bin/env python3
# Monto command

import getopt, multiprocessing, pathlib, os, psutil
import signal, subprocess, sys, tempfile
import montolib

# Main program

def main ():
    try:
        opts, args = getopt.getopt (sys.argv[1:], 'h', ['help'])
    except getopt.GetoptError as err:
        montolib.error (err)
        usage ()
        sys.exit (2)
    for o, a in opts:
        if o in ('-h', '--help'):
            usage ()
            sys.exit ()
        else:
            assert False, 'unhandled option'
    numargs = len (args)
    if numargs <= 1:
        if numargs == 0:
            command = 'start'
        elif numargs == 1:
            command = args[0]
        config = montolib.monto_read_config ()
        if command == 'restart':
            stop (config)
            start (config)
        elif command == 'start':
            start (config)
        elif command == 'status':
            status (config)
        elif command == 'stop':
            stop (config)
        else:
            montolib.error ('unknown command \'{0}\''.format (command))
            usage ()
    else:
        montolib.error ('too many commands')
        usage ()

def usage ():
    print ('usage: monto [-h] [command]')
    print ('  commands: restart, start (default), stop')

# Management of lock file that contains PIDs of broker and programs

def pid_file_path ():
    return pathlib.Path (os.path.join (tempfile.gettempdir (), 'monto.pids'))

def monto_is_running ():
    return pid_file_path ().is_file ()

def add_pid (pid):
    with pid_file_path ().open ('a') as file:
        file.write ('{0}\n'.format (pid))

def get_pids ():
    with pid_file_path ().open ('r') as file:
        contents = file.read ()
        pids = []
        for line in contents.split ('\n'):
            if line:
                try:
                    pid = int (line)
                    pids.append (pid)
                except ValueError:
                    montolib.error ('bad line in {0}: {1}'.format (pid_file_path (), line))
                    sys.exit (4)
        return pids

def kill_all_pids ():
    if sys.platform == "win32":
        sig = signal.SIGBREAK
    else:
        sig = signal.SIGTERM
    for pid in get_pids ():
        try:
            os.kill (pid, sig)
        except ProcessLookupError:
            montolib.error ('process {0!s} does not exist'.format (pid))

def remove_pid_file ():
    pid_file_path ().unlink ()

# Start command

def start (config):
    if monto_is_running ():
        print ('monto is already running')
    else:
        programs = []
        if 'programs' in config:
            programs = config['programs']
            for source in programs:
                run_command (config, source)
        report_start (len (programs))

def run_command (config, source):
    cwd = source.get ('directory', '.')
    if 'command' in source:
        try:
            process = subprocess.Popen (
                          args = source['command'],
                          cwd = cwd,
                          env = os.environ,
                          stderr = subprocess.DEVNULL,
                          stdin = subprocess.DEVNULL,
                          stdout = subprocess.DEVNULL
                      )
            add_pid (process.pid)
        except OSError as err:
            montolib.error ('can\'t run program {0}: {1}'.format (
                source['command'], err.strerror))
    else:
        print ('monto: source missing command:')
        print (source)
        sys.exit (6)

def report_start (numprograms):
    if numprograms == 0:
        print ('monto not started (no programs)')
    else:
        print ('monto started', end='')
        if numprograms > 1:
            plural = 's'
        else:
            plural = ''
        print (' ({0!s} program{1})'.format (numprograms, plural))

# Status command

def status (config):
    if monto_is_running ():
        print ('monto is running')
        pids = get_pids ()
        if len (pids) == 0:
            print ('  no programs')
        else:
            print ('  programs:')
            for pid in get_pids ():
                print ('    {0}'.format (process_desc (pid)))
    else:
        print ('monto is not running')

def process_desc (pid):
    try:
        p = psutil.Process (pid)
        cmdline = ' '.join (p.cmdline ())
        desc = cmdline[cmdline.rfind ('/') + 1:]
        return desc
    except psutil.NoSuchProcess:
        return 'no process ({0!s})'.format (pid)

# Stop command

def stop (config):
    if monto_is_running ():
        kill_all_pids ()
        remove_pid_file ()
        print ('monto has been stopped')
    else:
        print ('monto is not running')

# Startup

if __name__ == '__main__':
    main ()
