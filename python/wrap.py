#! /usr/bin/env python3
# Wrap a shell command as a Monto server

import getopt, os, pathlib, re, subprocess, sys, tempfile
import montolib

# Main program

def main ():
    try:
        opts, args = getopt.getopt (sys.argv[1:], 'v:h', ['verslang', 'help'])
    except getopt.GetoptError as err:
        error (err)
        sys.exit (2)
    verslang = None
    for o, a in opts:
        if o in ('-v', '--verslang'):
            verslang = a
        elif o in ('-h', '--help'):
            usage ()
            sys.exit ()
        else:
            assert False, 'unhandled option'
    if len (args) < 3:
        error ('not enough arguments')
    else:
        tmpfile = tempfile.mkstemp (suffix='.txt', text=True)
        wrap (verslang, args[0], args[1], args[2:], pathlib.Path (tmpfile[1]))
        os.unlink (tmpfile[1])

def error (msg):
    print ('wrap: {0!s}'.format (msg))
    usage ()

def usage ():
    print ('usage: wrap [-v verslang] product prodlang command args...')

# Wrapping

def wrap (verslang, product, prodlang, args, tmpfilepath):
    montolib.server (
        lambda change: run_command (product, prodlang, change, args, tmpfilepath),
        filter = verslang
    )

def run_command (product, prodlang, change, args, tmpfilepath):
    cmdstr = ' '.join (args)
    try:
        with tmpfilepath.open ('w') as file:
            file.write (change['contents'])
        tmpfilename = str (tmpfilepath)
        stdinfd = os.open (tmpfilename, os.O_RDONLY)
        cmdargs = make_command_args (change, args, tmpfilename)
        output = subprocess.check_output (args=cmdargs, stdin=stdinfd)
        os.close (stdinfd)
        return [(product, prodlang, output.decode (), True)]
    except subprocess.CalledProcessError as err:
        msg = err.output.decode ()
        return [(product, 'text', msg, True)]
    except Exception as err:
        msg = 'Command \'{0}\' failed: {1!s}'.format (cmdstr, err)
        return [(product, 'text', msg, True)]

def make_command_args (change, args, tmpfilename):
    return map (lambda arg: replace_arg (change, arg, tmpfilename), args)

def replace_arg (change, arg, tmpfilename):
    replacements = {
        "$CONTENTS": tmpfilename,
        "$LANGUAGE": change['language'],
        "$SELECTION": montolib.get_selection_text (change),
        "$SOURCE": change['source']
    }
    return re.sub (r'(\$[A-Z]+)', lambda m: replacements.get (m.group (0), m.group (0)), arg)

# Startup

if __name__ == '__main__':
    main ()
